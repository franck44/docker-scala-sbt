
[![Docker Automated build](https://img.shields.io/docker/automated/jrottenberg/ffmpeg.svg?maxAge=2592000?style=plastic)](https://bitbucket.org/franck44/docker-scala-sbt)

# docker-scala-sbt

Docker Image containing Scala 2.12.2, SBT 0.13.15 and OpenJDK8, gcc, g++ and make
